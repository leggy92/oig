#include <iostream>
#include <vector>
#include <limits>

class Apteka
{
public:
	Apteka(){}
	~Apteka(){}
	void wczytaj();
	long long unsigned int oblicz();
private:
	std::vector<long long unsigned int> kolejka;
};

void Apteka::wczytaj()
{
	int n, tmp;
	std::cin >> n;
	kolejka.resize(n);
	for(int i=0; i<n; i++)
	{
		std::cin >> tmp;
		kolejka[i] = tmp;
	}
}

long long unsigned int Apteka::oblicz()
{
	std::vector<long long unsigned int> zmiennicy;
	zmiennicy.push_back(kolejka.size()-1);
	int min = kolejka.back();
	for(int i = kolejka.size()-2 ; i >= 0 ; i--)
	{
		if(kolejka[i]< min)
		{
			min = kolejka[i];
			zmiennicy.push_back(i);
		}
	}
	zmiennicy.push_back(-1);

	long long unsigned  koszt = 0; 
	/*for(auto zmiennik : zmiennicy)
	{
		std::cout << zmiennik << " ";
	}
	std::cout << std::endl;*/
	
	for(int i=0; i<zmiennicy.size()-1; i++)
	{
		koszt+=kolejka[zmiennicy[i]]*(abs(zmiennicy[i]-zmiennicy[i+1]));
	}

	return koszt;
}

int main(int argc, char const *argv[])
{
	Apteka apteka;
	apteka.wczytaj();
	std::cout << apteka.oblicz() << std::endl;
	return 0;
}
